import {reducer, log} from "./helper/reducer";

var request = require('request');
var parsed = reducer(log);
var options = {
  'method': 'POST',
  'url': 'http://localhost:666',
  'headers': {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(parsed)
};
request(options, function (error, response) {
  if (error) throw new Error(error);
  console.log(response.body);
});
