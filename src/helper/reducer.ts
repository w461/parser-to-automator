//import { arrayBuffer } from "stream/consumers";
import { datetimeConverter } from "./datetimeConverter";
import { Nester } from "./nester";
var log = {
   config: {
     type: "CONFIG",
     service: "com.cplus.ia",
     time: "03:59:22.8990",
     date: "18.03.2021",
     url: "com.cplus.ia.Instance",
     tag: "SessionProvider",
     message: "service error",
     thread: "IA-Pool-Thread-12",
     thread_number: "24",
     logSource: "127.0.0.1:13217",
     exception:
       "com.sun.xml.internal.ws.client.ClientTransportException com.sun.xml.internal.ws.transport.http$",
     cause:
       "java.net.ConnectException java.net.PlainSocketImpl.socketConnect[PlainSocketImpl.java] <- java.n$",
     source: {
       url: "com.cplus.ia.Instance",
       tag: "SessionProvider",
       id: "160e99e0",
     },
     text: {
       errortype: "HTTP transport error",
       errorwhy: "java.net.ConnectException",
       errormsg: "Connection refused (Connection refused)",
       undefined: "Connection refused (Connection refused)",
     },
   },
   fine: {
     type: "FINE",
     service: "com.cplus.sdk.net",
     time: "04:24:40.0690",
     date: "18.03.2021",
     module: "com.cplus.sdk.net.Client",
     frame:
       "MDIwMEUyM0E0NDgxMjhFMTgwMDQwMDAwMDAwMDE2MDAwMDBBMTY0NjYxNjAxMTE4MDQyNDQwMzkwMDAwMDMxODA0MjQ0MDkyOTM2MTA0MjQ0MDAzMTgwMzE4NjAxNzAyMTAwMDMyMTMzNDQ2NjE2MDExMTgwNDI0NDA9NzAxMjEwMTE5ODExNDI5MDEwMDE4MTM5NjQ0NDhEQ1NEQjAwMTAwMDAwMDBEQ1NEQjAwMUJBTksgQlRQTiAtIFNNQVJUIERJR0lUQUwgQkFOSyAtIEpBS0FSVEEwNzYgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgMzYwMDEwMDAwMDAwMDAwMDAzMjEzMTE5MDAyMDUxMDEwMDEwNDAxMDk4ODk5OTAwMUMwMDMwMTQ=",
     thread: "IA-Pool-Thread-43",
     thread_number: "56",
     logSource: "127.0.0.1:13217",
     source: {
       transport: "TCP/IP",
       ipto: "10.1.66.23:48030",
       ipfrom: "10.1.180.45:3445",
       keepalive: "true",
       framer: "L2 L1",
     },
   },
  //hoho: "feeling aing jelek nih",
  info: {
    type: "INFO",
    service: "com.cplus.sdk.iso.module",
    time: "03:59:23.1400",
    date: "18.03.2021",
    message: `[Request:State]{"A4M",Version=10,Reject=0,MTI=800,[7]GMTTime='0317205919',[11]STAN=575593,[70]NetMgmt=401}`,
    protocol: "TranzWare-InterConnect",
    version: "v.2.115-11",
    logSource: "127.0.0.1:13217",
    client: {
      transport: "TCP/IP",
      to: "/127.0.0.1:46616",
      from: "/127.0.0.1:3999",
      keepAlive: "true",
      framer: "L2 L1 <###>",
    },
  },
};

export {log};
export function reducer(log: object) {
  let temp: any = log;
  let num = Object.keys(log).length;
  if (num === 0) {
    return {
      msg: "Log kosong",
    };
  } else if (num >= 1 && num <= 3) {
    var reduced: any;
    Object.keys(temp).forEach((alert_type: any) => {
      if (temp[alert_type].type === "INFO") {
        let buf = {
          type: temp[alert_type].type,
          from: temp[alert_type].logSource,
          timestamp: datetimeConverter(
            temp[alert_type].date,
            temp[alert_type].time
          ),
        };
        let reduced_temp = {
          [alert_type]: buf,
        };
        reduced = { ...reduced, ...reduced_temp };
      } else if (
        temp[alert_type].type === "CONFIG" ||
        temp[alert_type].type === "FINE"
      ) {
        let buf = {
          type: temp[alert_type].type,
          from: temp[alert_type].logSource,
          timestamp: datetimeConverter(
            temp[alert_type].date,
            temp[alert_type].time
          ),
        };
        let reduced_temp = {
          [alert_type]: buf,
        };
        reduced = { ...reduced, ...reduced_temp };
      } else {
        return {
          msg: "Status Alert tidak diketahui",
        };
      }
    });
    if (reduced === undefined) {
      return {
        msg: "Logging gak sesuai format, yang bener oii",
      };
    }
    return reduced;
  } else {
    return {
      msg: "Logging aneh",
    };
  }
}
console.log(reducer(log));

// if (num === 0) {
//   return {
//     msg: "Log kosong",
//   };
// } else if (num === 1) {
//   let key: any = Object.keys(temp)[0];
//   console.log(key);
//   if (temp[key].type === "CONFIG" || temp[key].type === "FINE") {
//     return {
//       type: temp[key].type,
//       from: temp[key].source_log,
//       timestamp: datetimeConverter(temp[key].date, temp[key].time),
//     };
//   } else if (temp[key].type === "INFO") {
//     return {
//       type: temp[key].type,
//       from: temp[key].source_log,
//       timestamp: datetimeConverter(temp[key].date, temp[key].time),
//     };
//   }
// } else if (num === 3 || num === 2) {
//   var reduced: any;
//   Object.keys(temp).forEach((alert_type: any) => {
//     if (temp[alert_type].type === "INFO") {
//       let buf = {
//         type: temp[alert_type].type,
//         from: temp[alert_type].source_log,
//         timestamp: datetimeConverter(
//           temp[alert_type].date,
//           temp[alert_type].time
//         ),
//       };
//       let reduced_temp = {
//         [alert_type]: buf,
//       };
//       reduced = { ...reduced, ...reduced_temp };
//     } else if (
//       temp[alert_type].type === "CONFIG" ||
//       temp[alert_type].type === "FINE"
//     ) {
//       let buf = {
//         type: temp[alert_type].type,
//         from: temp[alert_type].source_log,
//         timestamp: datetimeConverter(
//           temp[alert_type].date,
//           temp[alert_type].time
//         ),
//       };
//       let reduced_temp = {
//         [alert_type]: buf,
//       };
//       reduced = { ...reduced, ...reduced_temp };
//     } else {
//       return {
//         msg: "Status Alert tidak diketahui",
//       };
//     }
//   });
//   return reduced;
// } else {
//   return {
//     msg: "Logging aneh",
//   };
// }
