import { loadDefaultSync } from "grok-js";
import { Childs, Pattern } from "../constant/pattern";
import { Nester } from "./nester";

export function ParserGrok(inp: string) {
  let pattern: any = Pattern;
  let pat = loadDefaultSync();
  let exportParse: any = {};
  Object.keys(pattern).map(function (key, index) {
    let patt = pat.createPattern(pattern[key]);
    let log = patt.parseSync(inp);
    if (log !== null) {
      if (exportParse[key] == undefined) {
        exportParse[key] = {};
      }
      let child: any = Childs;
      if (child[key]) {
        child[key].map((value: any) => {
          log = Nester(log, value);
        });
      }
      exportParse[key] = log;
    }
  });
  return exportParse;
}
